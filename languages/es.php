<?php

$spanish = array(
	'gentelella:tabs:details' => 'Detalles',
        'gentelella:tabs:assignments' => 'Tareas',
        'gentelella:tabs:evaluations' => 'Evaluaciones',
        'gentelella:tabs:announcements' => 'Noticias',
        'gentelella:tabs:files' => 'M&oacute;dulos',
    
        'river:create:object:file' => '%s subi&oacute; el m&oacute;dulo: %s',
        'river:create:object:groupforumtopic' => '%s ha agregado la tarea: %s',
    
        'gentelella:assignment:title' => 'Tareas',
        'gentelella:assignment:create' => 'Crear',
        'gentelella:assignment:main' => 'Asignar',
        'gentelella:assignment:lower' => 'tareas a la clase',

        'gentelella:role:default' => 'Normal',
        'gentelella:role:student' => 'Estudiante',
        'gentelella:role:teacher' => 'Docente',
        'gentelella:role:profiles' => 'Perfil',
        
        'gentelella:profile_tabs:class_title' => 'Clases',
        'gentelella:profile_tabs:news_title' => 'Noticias Publicadas',
        'gentelella:profile_tabs:profile_title' => 'Informaci&oacute;n del usuario',
    
    
        'gentelella:evaluations:title' => 'Evaluaci&oacute;n',
        'gentelella:evaluations:new' => 'Nuevo',
        'gentelella:evaluations:main' => 'Crear',
        'gentelella:evaluations:lower' => 'una evaluaci&oacute;n',
    
    
        'gentelella:files:title' => 'M&oacute;dulos',
        'gentelella:files:new' => 'Subir',
        'gentelella:files:main' => 'Agregar',
        'gentelella:files:lower' => 'm&oacute;dulo a la clase',
    
    
        'gentelella:announcements:title' => 'Noticias',
        'gentelella:announcements:new' => 'Publicar',
        'gentelella:announcements:main' => 'Redactar',
        'gentelella:announcements:lower' => 'una noticia',
        
    
        'gentelella:evaluations:list' => 'Listado general',
        'gentelella:announcements:list' => 'Listado general',
        'gentelella:files:list' => 'Listado general',
    
        'gentelella:details:activity' => '&Uacute;ltimas actividades',
        'gentelella:details:options' => 'Opciones del docente',
        'gentelella:details:more' => 'Ver m&aacute;s actualizaciones de la clase',
    
        'gentelella:modal:title' => 'Opciones',
        'gentelella:modal:close' => 'Cerrar',
        'group_tools:action:invite:success:invite'=> "Ha agregado a %s usuarios (%s agregados y %s ya eran miembros)",
        'group_tools:action:invite:error:invite'=> "Ning&uacute;n usuario ha sido agregado (%s agregados, %s ya eran miembros)",
        'group_tools:action:invite:error:add'=> "Ning&uacute;n usuario fue agregado (%s agregados, %s ya eran miembros)",
        'group_tools:group:invite:add:confirm' => "Agregar usuarios a la clase?",
);

add_translation('es', $spanish);