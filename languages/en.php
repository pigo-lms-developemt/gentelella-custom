<?php

$english = array(
	'error:404:content' => 'Sorry. We could not find the page that you requested.',
        'gentelella:tabs:details' => 'Class details',
        'gentelella:tabs:assignments' => 'Assignments',
        'gentelella:tabs:evaluations' => 'Evaluations',
);

add_translation('en', $english);