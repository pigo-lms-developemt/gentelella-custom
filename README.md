Gentelella theme for Elgg 1.8
------------

This is a heavy customization of the Gentelella Admin Template Theme for elgg. This version has been ported to be
compatible with Elgg 1.8 while still having the original appearance and the responsive layout.

DESIGN

The theme is designed with these keywords in mind, mobile-first, minimalism, less is more or similar.
White space is considered a design element, and also provides space around links and content in general.

LAYOUT

Gentelella theme is responsive and adapts the layout to the viewing environment using media queries and fluid images.
Respond.js is used to enable responsiveness in browsers that don't support CSS3 media queries (IE 8).

Elgg css is extended by views/default/gentelella_theme/css, containing the media queries.

REQUIREMENTS

Place Gentelella theme at or near the bottom of the plugin list.

LICENSE INFORMATION

The Gentelella Admin Project is developed and maintained by Colorlib and Aigars Silkalns, and it is licensed under The MIT License (MIT). Which means that you can use, copy, modify, merge, publish, distribute, sublicense, and/or sell copies of the Software. But you always need to state that Colorlib is the original author of this template.
