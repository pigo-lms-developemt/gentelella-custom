<?php
/**
 * Elgg pageshell
 * The standard HTML page shell that everything else fits into
 *
 * @package Elgg
 * @subpackage Core
 *
 * @uses $vars['title']       The page title
 * @uses $vars['body']        The main content of the page
 * @uses $vars['sysmessages'] A 2d array of various message registers, passed from system_messages()
 */

// backward compatability support for plugins that are not using the new approach
// of routing through admin. See reportedcontent plugin for a simple example.
if (elgg_get_context() == 'admin') {
	if (get_input('handler') != 'admin') {
		elgg_deprecated_notice("admin plugins should route through 'admin'.", 1.8);
	}
	elgg_admin_add_plugin_settings_menu();
	elgg_unregister_css('elgg');
	echo elgg_view('page/admin', $vars);
	return true;
}

// render content before head so that JavaScript and CSS can be loaded. See #4032
$topbar = elgg_view('page/elements/topbar', $vars);
$messages = elgg_view('page/elements/messages', array('object' => $vars['sysmessages']));
$header = elgg_view('page/elements/header', $vars);
$body = elgg_view('page/elements/body', $vars);
$footer = elgg_view('page/elements/footer', $vars);

// Set the content type
header("Content-type: text/html; charset=UTF-8");
$site = elgg_get_site_entity();
$site_url = elgg_get_site_url();
$site_name = $site->name;

$lang = get_current_language();

?>
<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">
<html xmlns="http://www.w3.org/1999/xhtml" xml:lang="<?php echo $lang; ?>" lang="<?php echo $lang; ?>">
<head>
<?php echo elgg_view('page/elements/head', $vars); ?>
    
     <link href="<?php echo $site_url ?>mod/gentelella_theme/vendors/bootstrap/dist/css/bootstrap.min.css" rel="stylesheet">
    <!-- Font Awesome -->
    <link href="<?php echo $site_url ?>mod/gentelella_theme/vendors/font-awesome/css/font-awesome.min.css" rel="stylesheet">
    <!-- NProgress -->
    <link href="<?php echo $site_url ?>mod/gentelella_theme/vendors/nprogress/nprogress.css" rel="stylesheet">

    <!-- Custom Theme Style -->
    <link href="<?php echo $site_url ?>mod/gentelella_theme/vendors/build/css/custom.min.css" rel="stylesheet">
</head>
    <body class="nav-md">
        <div class="elgg-page-messages">
		<?php echo elgg_view('page/elements/messages', array('object' => $vars['sysmessages'])); ?>
	</div>
    <div class="container body">
      <div class="main_container">
        <div class="col-md-3 left_col">
          <div class=" scroll-view">
            <div class="navbar nav_title" style="border: 0; color:#f2f3f2; font-size: 26px; margin-top: 8px;">
                <center>
                    <a href="<?php echo $site_url ?>" style="color:white; text-decoration: none;">
              <i class="fa fa-bank"></i> <span><?php echo $site_name; ?></span> 
              </a>
              </center>
            </div>
              <?php 
                      
                      echo elgg_view('gentelella_theme/layouts/sidebar');
            ?>

              
              
          </div>
        </div>

        <!-- top navigation -->
        <div class="top_nav">
          <?php 
                      
                      echo elgg_view('gentelella_theme/layouts/topbar');
        ?>

        </div>
        <!-- /top navigation -->

        <!-- page content -->
        <div class="right_col" role="main">
          <div class="">
            <div class="page-title">
              <div class="title_left">
               <!-- <h3>Plain Page</h3>-->
              </div>

              <div class="title_right">
                <div class="col-md-5 col-sm-5 col-xs-12 form-group pull-right top_search">
                  <div class="input-group">
                     
                  </div>
                </div>
              </div>
            </div>

            <div class="clearfix"></div>

            <div class="row">
              <div class="col-md-12 col-sm-12 col-xs-12">
                <div class="x_panel">
                  <div class="x_title">
                    <h2><?php echo $site_name?></h2>
                     
                    <div class="clearfix"></div>
                  </div>
                  <div class="x_content">
                     <?php echo $body; ?>
                  </div>
                </div>
              </div>
            </div>
            
             <br />
<br /><br /><br /><br /><br />
          <div class="row">
<br /><br /><br />

            

          </div>


          </div>
        </div>
        <!-- /page content -->

        <!-- footer content -->
        <?php 
                      
                      echo elgg_view('gentelella_theme/layouts/footer');
        ?>
        <!-- /footer content -->
      </div>
    </div>

    <!-- jQuery -->
    <script src="<?php echo $site_url ?>mod/gentelella_theme/vendors/jquery/dist/jquery.min.js"></script>
         
    <!-- Bootstrap -->
    <script src="<?php echo $site_url ?>mod/gentelella_theme/vendors/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- FastClick -->
    <script src="<?php echo $site_url ?>mod/gentelella_theme/vendors/fastclick/lib/fastclick.js"></script>
    <!-- NProgress -->
    <script src="<?php echo $site_url ?>mod/gentelella_theme/vendors/nprogress/nprogress.js"></script>
    
       
    <!-- Custom Theme Scripts -->
    
    <script>
   var jQuery_2_2_3 = jQuery.noConflict(true);
   
   //var $j = jQuery.noConflict();
</script>
    <script src="<?php echo $site_url ?>mod/gentelella_theme/vendors/build/js/custom.js"></script>
    <script type="text/javascript" src="<?php echo $site_url ?>mod/gentelella_theme/vendors/embed/embed.js"></script>
       
  </body>

</html>