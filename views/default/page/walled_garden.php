<?php
/**
 * Walled garden page shell
 *
 * Used for the walled garden index page
 */

$is_sticky_register = elgg_is_sticky_form('register');
$wg_body_class = 'elgg-body-walledgarden';
if ($is_sticky_register) {
	$wg_body_class .= ' hidden';
}

// Set the content type
header("Content-type: text/html; charset=UTF-8");
?>