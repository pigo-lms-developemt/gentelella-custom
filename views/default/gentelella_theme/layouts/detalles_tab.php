<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$group = $vars['entity'];
$owner = $group->getOwnerEntity();
$site_url = elgg_get_site_url();
?>
<div role="tabpanel" class="tab-pane fade active in" id="tab_content_detalles" aria-labelledby="detalles-tab">
                                <div class="groups-stats">
                                    <p style="font-size: 16px;">
                                    <b>
                                        <?php echo elgg_echo("groups:owner"); ?>: 
                                    </b>
                                        <?php
                                                echo elgg_view('output/url', array(
                                                        'text' => $owner->name,
                                                        'value' => $owner->getURL(),
                                                        'is_trusted' => true,
                                                ));
                                        ?>
                                    </p>
                                    
                                    <p> 

                                        <?php  //echo $current_role->name;
                                            echo elgg_echo('groups:members') . ": " . $group->getMembers(0, 0, TRUE);
                                           
                                        ?>
                                    </p>
                                    
                                    
                                </div>
                                 
                                <p>
                                    <div class="alert alert-success alert-dismissible fade in" role="alert">

                                         
                                        <?php
                                        echo elgg_view('groups/profile/fields', $vars);
                                        ?>

                                    </div>
                                </p>      
                                 
                                  <table class="data table table-striped no-margin">
                              <thead>
                                <tr>
                                    
                                    <th><?php echo elgg_echo("gentelella:details:activity"); ?> </th>
                                   
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                   
                                  <td>
                                  <?php
                                    elgg_push_context('widgets');
                                    $db_prefix = elgg_get_config('dbprefix');
                                    $content = elgg_list_river(array(
                                    'limit' => 6,
                                    'pagination' => false,
                                    'joins' => array("JOIN {$db_prefix}entities e1 ON e1.guid = rv.object_guid"),
                                    'wheres' => array("(e1.container_guid = $group->guid)"),
                                    ));
                                    //$content = elgg_list_entities_from_relationship($options);
                                    echo $content;
                                    
                               
                                  ?>          
                                  </td>
                                   
                                </tr>
                                
                                <tr>
                                   
                                  <td>
                                      <a href="<?php echo $site_url;?>groups/activity/<?php echo $group->guid ?>">
                                      <button class="btn btn-primary pull-left" style="margin-right: 5px;"><i class="fa fa-history"></i>
                                           <?php echo elgg_echo('gentelella:details:more');?>
                                    </button>
                                  </td>
                                   
                                </tr>
                              </tbody>
                            </table>
                                 
                            
                                 
</div>