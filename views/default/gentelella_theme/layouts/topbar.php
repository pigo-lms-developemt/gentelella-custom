<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$site_url = elgg_get_site_url();
$username = elgg_get_logged_in_user_entity()->username;
?>
<div class="nav_menu">
            <nav>
              <div class="nav toggle">
                <a id="menu_toggle"><i class="fa fa-bars"></i></a>
              </div>
                <?php

                    if (elgg_is_logged_in()) 
                    {  

                ?>

              <ul class="nav navbar-nav navbar-right">
                <li class="">
                  <a href="javascript:;" class="user-profile dropdown-toggle" data-toggle="dropdown" aria-expanded="false">
                     
                    <?php echo elgg_get_logged_in_user_entity()->name ?>
                     
                    <span class=" fa fa-angle-down"></span>
                  </a>
                  <ul class="dropdown-menu dropdown-usermenu pull-right">
                    <li>
                        <a href="<?php echo $site_url; ?>profile/<?php echo $username ?>"> 
                            <?php echo elgg_echo('profile')?>
                        </a>
                    </li>
                     
                     
                    <li>
                        <a href="<?php echo $site_url; ?>action/logout">
                            <i class="fa fa-sign-out pull-right"></i> 
                                <?php echo elgg_echo('logout')?>
                        </a>
                    </li>
                  </ul>
                </li>

                <li role="presentation" class="dropdown">
                  <a href="<?php echo $site_url ?>messages/inbox/<?php echo $username ?>" class="info-number" aria-expanded="false">
                    <i class="fa fa-envelope-o"></i>
                    <span class="badge bg-green"></span>
                  </a>
                   
                </li>
              </ul>
                <?php
                
                }
                           
    
                ?>

                
            </nav>
          </div>