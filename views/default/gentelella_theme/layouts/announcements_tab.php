<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

$group = $vars['entity'];
$owner = $group->getOwnerEntity();
?>
<div role="tabpanel" class="tab-pane fade" id="tab_content_noticias" aria-labelledby="evaluaciones-tab">

                              
                            <?php

                             $options = array(
                                             'type' => 'object',
                                             'subtype' => 'blog',
                                             'container_guid' => $group->getGUID(),
                                             'limit' => 6,
                                             'full_view' => FALSE,
                                             'pagination' => true,
                                         );

                             $content_news = elgg_list_entities($options);

                            ?>
                        
                            <!-- start user projects -->
                            <table class="data table table-striped no-margin">
                              <thead>
                                <tr>
                                  <th><?php echo elgg_echo("gentelella:announcements:list"); ?></th>
                                   
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                    <td>
                                        <?php
                                        echo $content_news;
                                        ?>
                                        
                                    </td>
                                  
                                   
                                </tr>
                              
                              </tbody>
                            </table>
                            <!-- end user projects -->

</div>