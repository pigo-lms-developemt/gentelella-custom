<?php
/**
 * Elgg file upload/save form
 *
 * @package ElggFile
 */

// once elgg_view stops throwing all sorts of junk into $vars, we can use 
$title = elgg_extract('title', $vars, '');
$desc = elgg_extract('description', $vars, '');
$tags = elgg_extract('tags', $vars, '');
$access_id = elgg_extract('access_id', $vars, ACCESS_DEFAULT);
$container_guid = elgg_extract('container_guid', $vars);

//Custom variables
$site_url = elgg_get_site_url();
$ts = time();
$token = generate_action_token($ts);

if (!$container_guid) {
	$container_guid = elgg_get_logged_in_user_guid();
}
$guid = elgg_extract('guid', $vars, null);

if ($guid) {
	$file_label = elgg_echo("file:replace");
	$submit_label = elgg_echo('save');
} else {
	$file_label = elgg_echo("file:file");
	$submit_label = elgg_echo('upload');
}

?>
<form method="post" action="<?php echo $site_url;?>action/file/upload" enctype="multipart/form-data" class="elgg-form elgg-form-embed elgg-form-file-upload">
 <div id="demo-form2" data-parsley-validate class="form-horizontal form-label-left">
<div class="form-group">
	<label><?php echo $file_label; ?></label><br />
	<?php echo elgg_view('input/file', array('name' => 'upload', 'class'=>'col-md-6 col-sm-6 col-xs-12')); ?>
</div>
<div class="form-group">
	<label><?php echo elgg_echo('title'); ?></label><br />
	<?php echo elgg_view('input/text', array('name' => 'title', 'value' => $title, 'class'=>'col-md-6 col-sm-6 col-xs-12' )); ?>
</div>
 	 
 
 
<?php

$categories = elgg_view('input/categories', $vars);
if ($categories) {
	echo $categories;
}

?>
<div>
	<label><?php echo elgg_echo('access'); ?></label><br />
	<?php echo elgg_view('input/access', array('name' => 'access_id', 'value' => $access_id)); ?>
</div>
     
      <input type="hidden" name="__elgg_token" value="<?php echo $token; ?>"/>
      <input type="hidden" name="__elgg_ts" value="<?php echo $ts; ?>" />
<div class="elgg-foot form-group">
    </br>
<?php

echo elgg_view('input/hidden', array('name' => 'container_guid', 'value' => $container_guid));

if ($guid) {
	echo elgg_view('input/hidden', array('name' => 'file_guid', 'value' => $guid));
}

echo elgg_view('input/submit', array('value' => $submit_label));

?>
</div>
</div>
    </form>
    