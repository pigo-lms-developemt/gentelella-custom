<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$group = $vars['entity'];
$owner = $group->getOwnerEntity();
?>
<div role="tabpanel" class="tab-pane fade" id="tab_content_files" aria-labelledby="files-tab">

                              
                            <?php

                             $options_files = array(
                                             'type' => 'object',
                                             'subtype' => 'file',
                                             'container_guid' => $group->getGUID(),
                                             'limit' => 14,
                                             'full_view' => FALSE,
                                             'pagination' => true,
                                         );

                             $content_files = elgg_list_entities($options_files);

                            ?>
                        
                            <!-- start user projects -->
                            <table class="data table table-striped no-margin">
                              <thead>
                                <tr>
                                  <th><?php echo elgg_echo("gentelella:files:list"); ?></th>
                                   
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                    <td>
                                        <?php
                                        echo $content_files;
                                        ?>
                                        
                                    </td>
                                  
                                   
                                </tr>
                              
                              </tbody>
                            </table>
                            <!-- end user projects -->

</div>
