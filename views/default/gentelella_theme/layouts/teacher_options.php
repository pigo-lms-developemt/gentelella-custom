<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$group = $vars['entity'];
?>

<div class="divTable">
<div class="divTableBody">
<div class="divTableRow">
<?php
if ($vars['entity']->forum_enable == 'yes')
    {

?>
<div class="divTableCell">

<div class="widget widget_tally_box" >
                        <div class="x_panel ui-ribbon-container fixed_height_190"  >
                          <div class="ui-ribbon-wrapper">
                            <div class="ui-ribbon">
                             <?php echo elgg_echo("gentelella:assignment:title"); ?>
                            </div>
                          </div>
                          <div class="x_title">
                            
                            <div class="clearfix"></div>
                          </div>
                          <div class="x_content">

                            <div style="text-align: center; margin-bottom: 17px">
                             <a class="btn btn-app" href="<?php echo elgg_get_site_url(); ?>discussion/add/<?php echo $group->guid; ?>">
                      <i class="fa fa-edit"></i> <?php echo elgg_echo("gentelella:assignment:create"); ?>
                    </a>
                            </div>

                            <h3 class="name_title"><?php echo elgg_echo("gentelella:assignment:main"); ?></h3>
                            <p><?php echo elgg_echo("gentelella:assignment:lower"); ?></p>

                            <div class="divider"></div>

                            
                          </div>
        </div>
</div>
</div>

<?php
                               
    }
?>
    
 <?php

if ($vars['entity']->izapchallenge_enable == "yes") 
{
?>
<div class="divTableCell">
    
<div class="widget widget_tally_box" >
                        <div class="x_panel ui-ribbon-container fixed_height_190"  >
                          <div class="ui-ribbon-wrapper">
                            <div class="ui-ribbon">
                             <?php echo elgg_echo("gentelella:evaluations:title"); ?>
                            </div>
                          </div>
                          <div class="x_title">
                            
                            <div class="clearfix"></div>
                          </div>
                          <div class="x_content">

                            <div style="text-align: center; margin-bottom: 17px">
                             <a class="btn btn-app" href="<?php echo elgg_get_site_url(); ?>challenge/add/group:<?php echo $group->guid; ?>/">
                                 <i class="fa fa-folder-open"></i> <?php echo elgg_echo("gentelella:evaluations:new"); ?>
                             </a>
                            </div>

                            <h3 class="name_title">
                                <?php echo elgg_echo("gentelella:evaluations:main"); ?>
                            </h3>
                            <p><?php echo elgg_echo("gentelella:evaluations:lower"); ?></p>

                            <div class="divider"></div>

                            
                          </div>
        </div>
</div>
</div>
 

<?php
}
?>
   
    
<?php

    if ($group->file_enable == "yes") { 
?>
<div class="divTableCell">

    <div class="widget widget_tally_box" >
                        <div class="x_panel ui-ribbon-container fixed_height_190"  >
                          <div class="ui-ribbon-wrapper">
                            <div class="ui-ribbon">
                             <?php echo elgg_echo("gentelella:files:title"); ?>
                            </div>
                          </div>
                          <div class="x_title">
                            
                            <div class="clearfix"></div>
                          </div>
                          <div class="x_content">

                            <div style="text-align: center; margin-bottom: 17px">
                             <a class="btn btn-app" href="<?php echo elgg_get_site_url(); ?>file/add/<?php echo $group->guid; ?>/">
                      <i class="fa fa-file"></i> <?php echo elgg_echo("gentelella:files:new"); ?>
                    </a>
                            </div>

                            <h3 class="name_title"><?php echo elgg_echo("gentelella:files:main"); ?></h3>
                            <p><?php echo elgg_echo("gentelella:files:lower"); ?></p>

                            <div class="divider"></div>

                            
                          </div>
        </div>
</div>

</div>
    
<?php
}
?>    

    
<?php
		
if ($group->blog_enable == "yes") {
		
?>
    <div class="divTableCell">

    <div class="widget widget_tally_box" >
                        <div class="x_panel ui-ribbon-container fixed_height_190"  >
                          <div class="ui-ribbon-wrapper">
                            <div class="ui-ribbon">
                            <?php echo elgg_echo("gentelella:announcements:title"); ?>
                            </div>
                          </div>
                          <div class="x_title">
                            
                            <div class="clearfix"></div>
                          </div>
                          <div class="x_content">

                            <div style="text-align: center; margin-bottom: 17px">
                             <a class="btn btn-app" href="<?php echo elgg_get_site_url(); ?>blog/add/<?php echo $group->guid; ?>/">
                      <i class="fa fa-book"></i> <?php echo elgg_echo("gentelella:announcements:new"); ?>
                    </a>
                            </div>

                            <h3 class="name_title">
                                <?php echo elgg_echo("gentelella:announcements:main"); ?>
                            </h3>
                            <p>
                                <?php echo elgg_echo("gentelella:announcements:lower"); ?>
                            </p>

                            <div class="divider"></div>

                            
                          </div>
        </div>
</div>

</div>
    
<?php
}
?> 
</div>
</div>
</div>










  