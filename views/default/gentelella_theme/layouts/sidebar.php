<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$navbar = elgg_view('page/elements/navbar', $vars);
$site_url = elgg_get_site_url();
$site = elgg_get_site_entity();
$username = elgg_get_logged_in_user_entity()->username;
?>


            <div class="clearfix"></div>

            <!-- menu profile quick info -->
           
            <!-- /menu profile quick info -->

            <br />

            <!-- sidebar menu -->
            <div id="sidebar-menu" class="main_menu_side hidden-print main_menu">
              <div class="menu_section">
                <h3>General</h3>
                 <?php 
                   echo $navbar;
                    
                     
                    ?>

                
              </div>
               <?php
    if (elgg_is_logged_in()) {
    
    ?>
              <div class="menu_section">
                <h3>Options</h3>
                <ul class="nav side-menu">
                    <li><a href="<?php echo $site_url; ?>settings/user/<?php echo $username;?>">
                          <i class="fa fa-dashboard"></i>
                          <span class="label label-info "><?php echo elgg_echo('settings')?></span></a>
                  </li>
                    <?php 
                    if(elgg_is_admin_user(elgg_get_logged_in_user_entity()->guid))
                    {
                        
                    
                    ?>
                                     
                  <li><a href="<?php echo $site_url; ?>admin">
                          <i class="fa fa-cogs"></i>
                          <span class="label label-success "><?php echo elgg_echo('admin')?></span></a>
                  </li>
                  <?php
                    }
                   ?>       
                  <li><a href="<?php echo $site_url; ?>action/logout">
                          <i class="fa fa-laptop"></i>
                          <span class="label label-danger "><?php echo elgg_echo('logout')?></span></a>
                  </li>
                  
                  
                </ul>
              </div>
<?php
    }
?>

            </div>
            <!-- /sidebar menu -->

            <!-- /menu footer buttons -->
            <div class="sidebar-footer hidden-small">
              
            </div>
            <!-- /menu footer buttons -->