<?php

/* 
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
$group = $vars['entity'];
$owner = $group->getOwnerEntity();
?>
<div role="tabpanel" class="tab-pane fade" id="tab_content_evaluaciones" aria-labelledby="evaluaciones-tab">

                              
                            <?php

                             $options = array(
                                             'type' => 'object',
                                             'subtype' => GLOBAL_IZAP_CONTEST_CHALLENGE_SUBTYPE,
                                             'container_guid' => $group->getGUID(),
                                             'limit' => 14,
                                             'full_view' => false,
                                             'pagination' => false,
                                         );

                             $content_izap = elgg_list_entities($options);

                            ?>
                        
                            <!-- start user projects -->
                            <table class="data table table-striped no-margin">
                              <thead>
                                <tr>
                                  <th><?php echo elgg_echo("gentelella:evaluations:list"); ?></th>
                                   
                                </tr>
                              </thead>
                              <tbody>
                                <tr>
                                    <td>
                                        <?php
                                        echo $content_izap;
                                        ?>
                                        
                                    </td>
                                  
                                   
                                </tr>
                              
                              </tbody>
                            </table>
                            <!-- end user projects -->

</div>