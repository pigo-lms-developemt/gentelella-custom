<?php
/**
 * Profile info box
 */
$user = elgg_get_page_owner_entity();
$icon = elgg_view_entity_icon($user, 'large', array(
	'use_hover' => false,
	'use_link' => false,
));

$current_role = roles_get_role($user);
    $current_role_internal_name = $current_role->name;
    
    switch($current_role_internal_name)
    {
        case 'teacher':
            $current_role_name = elgg_echo("gentelella:role:teacher");
            break;
        
        case 'student':
            $current_role_name = elgg_echo("gentelella:role:student");
            break;
        
        default:
            $current_role_name = elgg_echo("gentelella:role:default");
    }
    
        
?>
<div class="x_content">
                    <div class="col-md-3 col-sm-3 col-xs-12 profile_left">
                      <div class="profile_img">
                        <div id="crop-avatar">
                          <!-- Current avatar -->
                          <?php echo $icon;?>
                          <!--<img class="img-responsive avatar-view" src="images/picture.jpg" alt="Avatar" title="Change the avatar">-->
                        </div>
                      </div>
                      <h3><?php echo $user->name;?></h3>
  <?php// echo elgg_view('profile/details'); ?>
                      <ul class="list-unstyled user_data">
                        <li>
                            <i class="fa fa-map-marker user-profile-icon"></i> 
                            <b>
                                <?php echo elgg_echo("gentelella:role:profiles");?>: 
                            </b> 
                                <?php echo $current_role_name; ?>
                        </li>

                        
                      </ul>

                      
                      <?php echo elgg_view('gentelella_theme/layouts/profile_menu'); ?>
                      <br />

                      <!-- start skills -->
                      
                    
                       
                      <!-- end of skills -->

                    </div>
                    <div class="col-md-9 col-sm-9 col-xs-12">

                     
                      <!-- start of user-activity-graph -->
                      
                      <!-- end of user-activity-graph -->

                      <div class="" role="tabpanel" data-example-id="togglable-tabs">
                        <ul id="myTab" class="nav nav-tabs bar_tabs" role="tablist">
                          <li role="presentation" class="active"><a href="#tab_content1" id="home-tab" role="tab" data-toggle="tab" aria-expanded="true">
                                 <?php echo elgg_echo("gentelella:profile_tabs:class_title");?> 
                              
                            </a>
                          </li>
                          <?php 
                                if($current_role_internal_name == 'teacher') 
                                {
                              ?>
                          <li role="presentation" class="">
                              <a href="#tab_content2" role="tab" id="profile-tab" data-toggle="tab" aria-expanded="false">
                                  <?php echo elgg_echo("gentelella:profile_tabs:news_title");?> 
                              </a>
                          </li>
                          <?php 
                                }
                              ?>
                          <li role="presentation" class="">
                              <a href="#tab_content3" role="tab" id="profile-tab2" data-toggle="tab" aria-expanded="false">
                              <?php echo elgg_echo("gentelella:profile_tabs:profile_title");?>    
                              </a>
                          </li>
                        </ul>
                        <div id="myTabContent" class="tab-content">
                          <div role="tabpanel" class="tab-pane fade active in" id="tab_content1" aria-labelledby="home-tab">

                            <!-- start recent activity -->
                             <?php
                             
                             $num = 12;

$options = array(
	'type' => 'group',
	'relationship' => 'member',
	'relationship_guid' => $user->guid,
	'limit' => $num,
	'full_view' => FALSE,
	'pagination' => FALSE,
);
$content_classrooms = elgg_list_entities_from_relationship($options);

echo $content_classrooms;
                   
if ($content_classrooms) {
	$url = "groups/member/" . elgg_get_page_owner_entity()->username;
	$more_link = elgg_view('output/url', array(
		'href' => $url,
		'text' => elgg_echo('groups:more'),
		'is_trusted' => true,
	));
	echo "<span class=\"elgg-widget-more\">$more_link</span>";
} else {
	echo elgg_echo('groups:none');
}
                             ?>
                            <!-- end recent activity -->

                          </div>
                             <?php 
                                if($current_role_internal_name == 'teacher') 
                                {
                              ?>
                          <div role="tabpanel" class="tab-pane fade" id="tab_content2" aria-labelledby="profile-tab">

                            <!-- start user projects -->
                             <?php
                             
                             

                            $news_options = array(
                                    'type' => 'object',
                                    'subtype' => 'blog',
                                    'container_guid' => $user->guid,
                                    'limit' => 12,
                                    'full_view' => FALSE,
                                    'pagination' => FALSE,
                            );
                            $news_content = elgg_list_entities($news_options);

                            echo $news_content;

                            if ($news_content) {
                                    $blog_url = "blog/owner/" . elgg_get_page_owner_entity()->username;
                                    $more_link = elgg_view('output/url', array(
                                            'href' => $blog_url,
                                            'text' => elgg_echo('blog:moreblogs'),
                                            'is_trusted' => true,
                                    ));
                                    echo "<span class=\"elgg-widget-more\">$more_link</span>";
                            } else {
                                    echo elgg_echo('blog:noblogs');
                            }
                             
                             ?>
                            <!-- end user projects -->

                          </div> <?php 
                                }
                              ?>
                          <div role="tabpanel" class="tab-pane fade" id="tab_content3" aria-labelledby="profile-tab">
                            <p><?php echo elgg_view('profile/details'); ?></p>
                             <?php echo $current_role_name; ?>
                          </div>
                        </div>
                      </div>
                    </div>
                  </div>
<div class="profile elgg-col-2of3">
	<div class="elgg-inner clearfix">
		<?php// echo elgg_view('profile/owner_block'); ?>
		<?php //echo elgg_view('profile/details'); ?>
	</div>
</div>