<?php
/**
 * Group profile summary
 *
 * Icon and profile fields
 *url(<?php echo elgg_get_site_url(); ?>_graphics/elgg_sprites.png)
 * @uses $vars['group']
 */

if (!isset($vars['entity']) || !$vars['entity']) {
	echo elgg_echo('groups:notfound');
	return true;
}

$group = $vars['entity'];
$owner = $group->getOwnerEntity();

 $user = elgg_get_logged_in_user_entity();
 
    $current_role = roles_get_role($user);
    $current_role_name = $current_role->name;
   // echo $current_role->name;

if (!$owner) {
	// not having an owner is very bad so we throw an exception
	$msg = elgg_echo('InvalidParameterException:IdNotExistForGUID', array('group owner', $group->guid));
	throw new InvalidParameterException($msg);
}

?>
<head>
	
        
        
        <meta name="viewport" content="width=device-width, initial-scale=1.0"> 
 
        
        <style>
          .divTable{
	display: table;
	width: 100%;
}
.divTableRow {
	display: table-row;
}
.divTableHeading {
	 
	display: table-header-group;
}
.divTableCell, .divTableHead {
	 
	display: table-cell;
	padding: 3px 10px;
}
.divTableHeading {
	 
	display: table-header-group;
	font-weight: bold;
}
.divTableFoot {
	 
	display: table-footer-group;
	font-weight: bold;
}
.divTableBody {
	display: table-row-group;
}
            
        </style>	
    </head>

<div class="groups-profile clearfix elgg-image-blocks">
	<div class="elgg-image">
		<div class="groups-profile-icon" >
                   
			<center>
				 
			 
				
			
				
				
				
				
				<div class="sp-content">
					  
						  <?php echo elgg_view_entity_icon($group, 'medium'); ?> 
						 
					 
				</div><!-- sp-content -->
                                </br>
				 
                                
                                <?php 
                                    echo elgg_view('gentelella_theme/layouts/tab_panel', $vars);
                                ?>
                        
                        </center>
                        <div id="myTabContent" class="tab-content">
                            
                              <?php
                                       echo elgg_view('gentelella_theme/layouts/detalles_tab', $vars);
                              ?>
                            
                            
                             <?php
                                if ($vars['entity']->forum_enable == 'yes')
                                {
                                
                                    echo elgg_view('gentelella_theme/layouts/tareas_tab', $vars);

                                }
                            ?>
                            
                             <?php
		       
                                if ($vars['entity']->izapchallenge_enable == "yes") 
                                {

                                    echo elgg_view('gentelella_theme/layouts/evaluations_tab', $vars);

                                }
                            ?>
                            
                            
                            
                            <?php
		       
                                if ($vars['entity']->blog_enable == "yes") 
                                {

                                    echo elgg_view('gentelella_theme/layouts/announcements_tab', $vars);

                                }
                            ?>
                            
                            
                            <?php
		       
                                if ($vars['entity']->file_enable == "yes") 
                                {

                                    echo elgg_view('gentelella_theme/layouts/documents_tab', $vars);

                                }
                            ?>
                            
                            
                        </div>
                      </div>
			</div><!-- sp-slideshow -->
			<?php
				// we don't force icons to be square so don't set width/height
			//	echo elgg_view_entity_icon($group, 'master', array(
			//		'href' => '',
			//		'width' => '520',
			//		'height' => '346',
			//		'align' =>'center',
			//	)); 
			//
			?>
			
		</div>
		
	

	<div class="groups-profile-fields elgg-body">
            <?php
       //  echo elgg_view('gentelella_theme/layouts/teacher_options', $vars);
            if($current_role_name == 'teacher') 
                                {
?>
            
            <center>
            <button type="button" class="btn btn-info btn-lg" data-toggle="modal" data-target=".bs-example-modal-lg">
            <?php echo elgg_echo("gentelella:details:options"); ?>
            </button>
            </center>
             <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-hidden="true">
                    <div class="modal-dialog modal-lg">
                      <div class="modal-content">

                        <div class="modal-header">
                          <button type="button" class="close" data-dismiss="modal"><span aria-hidden="true">×</span>
                          </button>
                          <h4 class="modal-title" id="myModalLabel">
                              <?php echo elgg_echo("gentelella:modal:title"); ?>
                          </h4>
                        </div>
                        <div class="modal-body">
                         
                          <p>
                            <?php
                                      echo elgg_view('gentelella_theme/layouts/teacher_options', $vars);
                            ?>
                              
                          </p>
                        </div>
                        <div class="modal-footer">
                          <button type="button" class="btn btn-default" data-dismiss="modal">
                              <?php echo elgg_echo("gentelella:modal:close"); ?>
                          </button>
                         
                        </div>

                      </div>
                    </div>
                  </div>
            <?php 
                                }
                              ?>
	</div></br>

       
            
<?php
?>

